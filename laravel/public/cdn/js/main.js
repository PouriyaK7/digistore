let sidebar = document.getElementById('sidebar');
let shadow = document.getElementById('sidebar-shadow');
$('#hamburger-header').on('click', function () {
    sidebar.style.width = '50%';
    shadow.style.width = '100%';
    sidebar.style.display = 'block';
    shadow.style.display = 'block';
});

$('#sidebar-shadow').on('click', function () {
    sidebar.style.width = '0';
    shadow.style.display = 'none';
});
