<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $guarded = ['id'];
    protected $table = 'stores';
    public $timestamps = true;
}
