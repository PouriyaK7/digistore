<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GeneralController extends Controller
{
    public function isLogin($type = null) {
        if ($type != null) {
            if ($type == 'admin') if (Auth::user()->type == 10) return 1; else return 0;
            elseif ($type == 'store') if (Auth::user()->type != 10 || Auth::user()->type != 0) return 1; else return 0;
        }
        return 1;
    }

    public function checkLogin() {
        if (Auth::user()->end_date < Carbon::now()) Auth::logout();
    }

    public function my_orders() {
        if ($this->isLogin('store')) {
            $orders = Order::where('store', '=', Auth::user()->store)
                ->get();
        }
    }

    public function get_this_months_orders() {
        $current_month = date('m', time());
        $start_date = '2020-' . $current_month . '-01';
        $end_date = '2020-' . $current_month . '-30';
        $last_month = $current_month - 1;
        $orders = Order::whereBetween('created_at', [$start_date, $end_date])
            ->get();
        return [
            'current_month' => $current_month,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'last_month' => $last_month,
        ];
    }

    public function add_order(Request $request) {
        if ($this->isLogin('store')) {
            $inputs = $request->except('_token');
            $price = 0;
            $description = $inputs['description'];
            if ($inputs['manual-price'] == 'OK') $price = $inputs['price'];
            else {
                $product = Product::find($inputs['product']);
                $price = $product->sell_price * $inputs['count'];
            }
            return Order::create([
                'store' => Auth::user()->store,
                'title' => isset($product)? $product->name: $inputs['title'],
                'price' => $price,
                'description' => $description,
            ])? 1: 0;
        }
        return 0;
    }

    public function add_product(Request $request) {
        if ($this->isLogin('store')) {
            $inputs = $request->except('_token');
            $product_id =  Product::create([
                'store' => Auth::user()->store,
                'name' => $inputs['name'],
                'buy_price' => $inputs['buy_price'],
                'sell_price' => $inputs['sell_price'],
            ]);

            $product = Product::find($product_id->id);
            $product->slug = $product->name . '-' . $product->id;
            return $product->save()? 1: 0;
        }
        return 0;
    }

    public function stats_page($type, $number) {}

    public function products_page($slug) {}

    public function today_and_orders_page() {}

    public function single_product($slug) {}

    public function orders_list() {}

    public function today_orders() {}

    public function add_store(Request $request) {
        if ($this->isLogin('admin')) {
            $inputs = $request->except('_token');
            $store_id = Store::create([
                'name' => $inputs['name'],
                'type' => $inputs['type'],
                'address' => $inputs['address'],
                'location' => $inputs['location'],
                'phone_number' => $inputs['phone_number'],
                'description' => $inputs['description'],
                'city' => $inputs['city'],
            ]);
            $store = Store::find($store_id->id);
            $store->slug = $store->name . '-' . $store->id;
            return $store->save()? 1: 0;
        }
        return 0;
    }


}
