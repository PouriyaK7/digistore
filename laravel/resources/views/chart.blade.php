<!DOCTYPE HTML>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        shit
    </title>
    <link rel="stylesheet" href="{{ URL::to('cdn/css/dashboard.css') }}">
    <link rel="stylesheet" href="{{ URL::to('cdn/css/style.css') }}">

    <script>
        window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "light2",
                lines: false,
                title:{
                    text: "امروز"
                },
                axisY:{
                    includeZero: false
                },
                data: [{
                    type: "line",
                    indexLabelFontSize: 16,
                    dataPoints: [
                        { y: 450, label: 'fuck' },
                        { y: 414},
                        { y: 520 },
                        { y: 460 },
                        { y: 450 },
                        { y: 500 },
                        { y: 480 },
                        { y: 480 },
                        { y: 410 , indexLabel: "\u2193 lowest",markerColor: "DarkSlateGrey", markerType: "cross" },
                        { y: 500 },
                        { y: 480 },
                        { y: 510 },
                        { y:600, label: 'hello', indexLabel: "\u2191 highest",markerColor: "red", markerType: "triangle" }
                    ]
                }]
            });
            chart.render();

        }
    </script>
</head>
<body style="background: #eee;margin: 0">
<div class="sidebar-shadow" id="sidebar-shadow"></div>
<div class="sidebar" id="sidebar">
    <a href="#">خانه</a>
    <a href="#">یچیزی</a>
    <a href="#">ورود</a>
    <a href="#">ارتباط با ما</a>
</div>
<div class="eshop-header">
    <nav class="top-menu">
        <a href="#">خانه</a>
        <a href="#">یچیزی</a>
        <a href="#">ورود</a>
        <a href="#">ارتباط با ما</a>
        <span id="hamburger-header">&#9776;</span>
    </nav>
    <img src="{{ URL::to('cdn/img/logo.png') }}"  style="position: absolute;left: 0;height: 100%; top: 0">
</div>
<div style="box-shadow: 1px 1px 3px rgba(0,0,0,.3);margin-top: 7px;width: 100%;">
    <div id="chartContainer" style="height: 300px;direction: ltr"></div>
</div>

<section class="wrapper" style="overflow: hidden">
    <div class="row card_row">
        {{--        <h1 class="column h6 color_label">Some Category</h1>--}}
        <div class="column half_whole">
            <article class="card box_panel">
                <label class="card_label">
                    اضافه کردن درخواست
                </label>
                <section class="card_body">
                    <div class="graph">
                        <div class="knob_data">89<span class="txt_smaller">%</span></div>
                        <svg class="graph_media" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 266.4 266.4">
                            <g>
                                <path class="st0" d="M130.5,32.2V0c-53.3,1.1-99,33.5-119.3,79.6l30,11.8C56.7,57.3,90.7,33.3,130.5,32.2z"/>
                                <path class="st1" d="M133.2,0c-0.9,0-1.8,0-2.7,0v32.2c0.9,0,1.8,0,2.7,0c55.8,0,101,45.2,101,101s-45.2,101-101,101
                  s-101-45.2-101-101c0-14.9,3.2-29,9-41.7l-30-11.8C4,96,0,114.1,0,133.2c0,73.6,59.6,133.2,133.2,133.2s133.2-59.6,133.2-133.2
                  S206.7,0,133.2,0z"/>
                            </g>
                        </svg>
                    </div>
                </section>
                <section class="card_more">
                    <div class="color_label card_more_content card_division">
                        Maecenas sed diam eget risus varius blandit sit amet non magna. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                    </div>
                    <a class="icon icon_after card_more_button button_soft"></a>
                </section>
                <section class="stats stats_row">
                    <div class="stats_item half_whole small_whole">
                        <div class="txt_faded">
                            <label class="txt_label space_n_b">
                                Goal
                            </label>
                            <div class="txt_serif stats_item_number txt_success">
                                85<span class="txt_smaller">%</span>
                            </div>
                        </div>
                    </div>
                    <div class="stats_item half_whole">
                        <div class="txt_faded">
                            <label class="txt_label space_n_b">
                                Red Line
                            </label>
                            <div class="txt_serif stats_item_number txt_error">
                                80<span class="txt_smaller">.2%</span>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
        </div>
        <div class="column half_whole">
            <article class="card box_panel">
                <label class="card_label">
                    لیست فروش امروز
                </label>
                <section class="card_body">
                    <div class="graph">
                        <div class="txt_warn graph_data txt_serif">350,000 <span style="font-size: 20px;display: block;font-family: vazir">تومان سود</span></div>

                    </div>
                </section>
                <section class="card_more">
                    <div class="color_label card_more_content card_division">
                        Maecenas sed diam eget risus varius blandit sit amet non magna. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
                    </div>
                    <a class="icon icon_after card_more_button button_soft"></a>
                </section>
                <section class="stats stats_row" style="text-align: center">
                    <div class="stats_item half_whole" style="display: block;margin: auto">
                        <div class="txt_faded">
                            <label class="txt_label space_n_b" style="font-family: vazir">
                                تعداد فروش
                            </label>
                            <div class="txt_serif stats_item_number txt_success">
                                37
                            </div>
                        </div>
                    </div>
                </section>
            </article>
        </div>
    </div>
</section>


<script src="{{ URL::to('cdn/js/jquery.min.js') }}"></script>
<script src="{{ URL::to('cdn/js/chart.min.js') }}"></script>

<script>
    $(function(){
        $('.card_more_button').on('click',
            function() {
                $(this).closest('.card').toggleClass('card_full');
                $(this).siblings('.card_more_content').slideToggle('fast');
                $(this).toggleClass('flipY');
            }
        )
    }(jQuery));
</script>

<script src="{{ URL::to('cdn/js/main.js') }}"></script>
</body>
</html>
